package com.blacktobacco.recursos_practico;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String firstElementText = getResources().getStringArray(R.array.unicorns)[0];
        String secondElementText = getResources().getStringArray(R.array.unicorns)[1];
        String thirdElementText = getResources().getStringArray(R.array.unicorns)[2];

        TextView firstElement = (TextView)findViewById(R.id.firstElementTV);
        TextView secondElement = (TextView)findViewById(R.id.secondElementTV);
        TextView thirdElement = (TextView)findViewById(R.id.thirdElementTV);

        firstElement.setText(firstElementText);
        secondElement.setText(secondElementText);
        thirdElement.setText(thirdElementText);

        Log.i("array", "firstElement: " + firstElementText);
        Log.i("array", "secondElement: " + secondElementText);
        Log.i("array", "thirdElement: " + thirdElementText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
